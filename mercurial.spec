Name:           mercurial
Version:        6.9.1
Release:        1
Summary:        Source control management tool
License:        GPL-2.0-or-later
URL:            https://www.selenic.com/mercurial/
Source0:        https://www.selenic.com/mercurial/release/%{name}-%{version}.tar.gz

BuildRequires:  gcc python3 python3-devel emacs-nox emacs-el pkgconfig gettext python3-docutils
Requires:       python3 emacs-filesystem tk
Provides:       hg = %{version}-%{release} emacs-mercurial <= 3.4.1 emacs-mercurial-el <= 3.4.1
Provides:       mercurial-hgk mercurial-chg
Obsoletes:      emacs-mercurial <= 3.4.1 emacs-mercurial-el <= 3.4.1
Obsoletes:      mercurial-hgk mercurial-chg

%description
Mercurial is a free, distributed source control management tool.
It efficiently handles projects of any size and offers an easy and intuitive interface.

%package_help

#Build sections
%prep
%autosetup -n %{name}-%{version} -p1

sed -i 's|python|python3|' %{_builddir}/%{name}-%{version}/Makefile

%build
%make_build

pushd contrib/chg
make
popd

%install
%{__python3} setup.py install -O1 --root %{buildroot} --prefix %{_prefix} --record=%{name}.files
make install-doc DESTDIR=%{buildroot} MANDIR=%{_mandir}

grep -v -e 'hgk.py*' -e %{python3_sitearch}/mercurial/ -e %{python3_sitearch}/hgext/ < %{name}.files > %{name}-base.files
grep 'hgk.py*' < %{name}.files > %{name}-hgk.files

install -D -m 755 contrib/hgk %{buildroot}%{_libexecdir}/mercurial/hgk
install -m 755 contrib/hg-ssh %{buildroot}%{_bindir}

mkdir -p %{buildroot}%{bash_completions_dir}
install -m 644 contrib/bash_completion %{buildroot}%{bash_completions_dir}/hg

mkdir -p %{buildroot}%{zsh_completions_dir}
install -m 644 contrib/zsh_completion %{buildroot}%{zsh_completions_dir}/_mercurial

mkdir -p %{buildroot}%{_emacs_sitelispdir}/mercurial

pushd contrib
for file in mercurial.el mq.el; do
  #emacs -batch -l mercurial.el --no-site-file -f batch-byte-compile $file
  %{_emacs_bytecompile} $file
  install -p -m 644 $file ${file}c %{buildroot}%{_emacs_sitelispdir}/mercurial
  rm ${file}c
done
popd

pushd contrib/chg
make install DESTDIR=%{buildroot} PREFIX=%{_usr} MANDIR=%{_mandir}/man1
popd

mkdir -p %{buildroot}/%{_sysconfdir}/mercurial/hgrc.d

touch mercurial-site-start.el
mkdir -p %{buildroot}%{_emacs_sitestartdir} && install -m644 mercurial-site-start.el %{buildroot}%{_emacs_sitestartdir}

cat >hgk.rc <<EOF
[extensions]
# enable hgk extension ('hg help' shows 'view' as a command)
hgk=

[hgk]
path=%{_libexecdir}/mercurial/hgk
EOF
install -m 644 hgk.rc %{buildroot}/%{_sysconfdir}/mercurial/hgrc.d

cat > certs.rc <<EOF
# see: http://mercurial.selenic.com/wiki/CACertificates
[web]
cacerts = /etc/pki/tls/certs/ca-bundle.crt
EOF
install -m 644 certs.rc %{buildroot}/%{_sysconfdir}/mercurial/hgrc.d

mv %{buildroot}%{python3_sitearch}/mercurial/locale %{buildroot}%{_datadir}/locale
rm -rf %{buildroot}%{python3_sitearch}/mercurial/locale

%find_lang hg

grep -v locale %{name}-base.files > %{name}-base-filtered.files

%files -f %{name}-base-filtered.files -f hg.lang
%doc CONTRIBUTORS COPYING doc/README doc/hg*.txt doc/hg*.html *.cgi contrib/*.fcgi contrib/*.wsgi contrib/*.svg
%config(noreplace) %{_sysconfdir}/mercurial/hgrc.d/certs.rc
%{_sysconfdir}/mercurial/hgrc.d/hgk.rc
%dir %{_sysconfdir}/mercurial
%dir %{_sysconfdir}/mercurial/hgrc.d
%{_bindir}/hg-ssh
%{_bindir}/chg
%{bash_completions_dir}/*
%{zsh_completions_dir}/*
%{_libexecdir}/mercurial/
%{python3_sitearch}/mercurial
%{python3_sitearch}/hgext
%{_emacs_sitelispdir}/mercurial
%{_emacs_sitestartdir}/*.el

%files help
%attr(644,root,root)
%{_mandir}/man?/*

%changelog
* Fri Jan 17 2025 Funda Wang <fundawang@yeah.net> - 6.9.1-1
- update to 6.9.1

* Fri Nov 22 2024 Funda Wang <fundawang@yeah.net> - 6.9-1
- update to 6.9

* Thu Aug 15 2024 maqi <maqi@uniontech.com> - 6.8.1-1
- update to 6.8.1

* Tue Aug  6 2024 dillon chen <dillon.chen@gmail.com> - 6.8-1
- update to 6.8

* Tue Feb 19 2024 maqi <maqi@uniontech.com> - 6.6.1-1
- update to 6.6.1

* Wed Jul 12 2023 dillon chen <dillon.chen@gmail.com> - 6.5-1
- update to 6.5

* Tue Feb 07 2023 wangjunqi <wangjunqi@kylinos.cn> - 6.3.2-1
- update to 6.3.2

* Mon Jun 27 2022 dillon chen <dillon.chen@gmail.com> - 6.1.3-1
- update to 6.1.3

* Mon Feb 21 2022 panxiaohe <panxh.life@foxmail.com> - 6.0.3-1
- Upgrade to 6.0.3

* Sat Jan 23 2021 zoulin <zoulin13@huawei.com> - 5.6.1-1
- Upgrade to 5.6.1

* Wed Jun 17 2020 Shinwell Hu <huxinwei@huawei.com> - 5.4.1-1
- Upgrade to 5.4.1

* Thu Apr 23 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.1-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix the problems detected by oss-fuzz

* Thu Jan  9 2020 JeanLeo <liujianliu.liu@huawei.com> - 5.1-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:update software package

* Wed Sep 04 2019 openEuler Buildteam <buildteam@openeuler.org> - 5.1-1
- Package init
